# GitHub Actions on Fargate

## Summary
Run GitHub Actions using AWS Fargate

## Running

### Locally
From the `docker` directory:
* `docker build -t github-actions .`
  * On a Mac (M* Chip):
    * `docker build -t github-actions . --platform linux/amd64`
* Run Image with a Bash Shell (Testing):
  * `docker run --platform linux/amd64 --name github-actions -it github-actions:latest bash`
* `docker run --env ORGANIZATION=${ORGANIZATION:?} --env ACCESS_TOKEN=${ACCESS_TOKEN:?} github-actions:latest`
  * On a Mac (M* Chip):
    * `docker run --platform linux/amd64 --env ORGANIZATION=${ORGANIZATION:?} --env ACCESS_TOKEN=${ACCESS_TOKEN:?} github-actions:latest`

## Pipeline Setup

### Required Environment Variables
The following variables are required by the pipeline in order to function:
* `AWS_ACCESS_KEY_ID`
  * The AWS access key ID that's created from the Terraform
  * It's part of the output
* `AWS_SECRET_ACCESS_KEY`
  * The AWS secret access key that's created from the Terraform
  * Also part of the output
* `CLUSTER_NAME`
  * The name of the ECS cluster that's created
  * It'll be in the form of: `${var.project_name}-github-actions`
* `ECR_URL`
  * The URL for the ECR repository
* `ECS_SERVICE_NAME`
  * The name of the ECS service name

## Documentation Used
* [Official Docs](https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners)
* [Blog on testdriven.io from Michael Herman](https://testdriven.io/blog/github-actions-docker/)
* [GitHub Docs on Self-Hosted Runner Groups for An Organization](https://docs.github.com/en/github-ae@latest/actions/hosting-your-own-runners/managing-access-to-self-hosted-runners-using-groups#creating-a-self-hosted-runner-group-for-an-organization)