locals {
  availability_zones = [
    "${var.aws_region}a",
    "${var.aws_region}b",
    "${var.aws_region}c",
    "${var.aws_region}d",
    "${var.aws_region}e",
    "${var.aws_region}f",
  ]
}

resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "${var.project_name}-vpc"
  }
}

resource "aws_eip" "nat_gateway_ips" {
  count = var.public_subnet_count
  vpc   = true
}

resource "aws_subnet" "public_subnets" {
  count             = var.public_subnet_count
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = "10.0.${count.index + 1}.0/24"
  availability_zone = local.availability_zones[count.index % var.num_availability_zones]
  tags = {
    Name = "${var.project_name}-public-subnet-${count.index}"
  }
}

resource "aws_subnet" "private_subnets" {
  count             = var.private_subnet_count
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = "10.0.${var.public_subnet_count + count.index + 1}.0/24"
  availability_zone = local.availability_zones[count.index % var.num_availability_zones]
  tags = {
    Name = "${var.project_name}-private-subnet-${count.index}"
  }
}

resource "aws_nat_gateway" "nat_gateways" {
  count         = var.public_subnet_count
  allocation_id = aws_eip.nat_gateway_ips[count.index].id
  subnet_id     = aws_subnet.public_subnets[count.index].id
  depends_on = [
    aws_internet_gateway.internet_gateway
  ]
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "${var.project_name}-internet-gateway"
  }
}

resource "aws_route_table" "internet_route" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    gateway_id = aws_internet_gateway.internet_gateway.id
    cidr_block = "0.0.0.0/0"
  }
}

resource "aws_route_table" "nat_routes" {
  vpc_id = aws_vpc.main_vpc.id
  count  = var.public_subnet_count

  route {
    nat_gateway_id = aws_nat_gateway.nat_gateways[count.index].id
    cidr_block     = "0.0.0.0/0"
  }
}

resource "aws_route_table_association" "public" {
  count          = var.public_subnet_count
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.internet_route.id
}

resource "aws_route_table_association" "private" {
  count          = var.private_subnet_count
  subnet_id      = aws_subnet.private_subnets[count.index].id
  route_table_id = aws_route_table.nat_routes[count.index].id
}