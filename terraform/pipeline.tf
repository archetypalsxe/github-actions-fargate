locals {
  cicd_user_keys = {
    "${aws_iam_access_key.cicd_keys.id}" = "${aws_iam_access_key.cicd_keys.secret}"
  }
}

resource "aws_iam_user" "cicd_user" {
  name = "${var.project_name}-cicd"
  path = "/"
}

resource "aws_iam_access_key" "cicd_keys" {
  user   = aws_iam_user.cicd_user.name
  status = "Active"
}

data "aws_iam_policy_document" "ecr_access" {
  statement {
    sid = "EcrAuthorization"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer",
    ]
    resources = [
      "*",
    ]
  }

  statement {
    sid = "ECSDeployment"
    actions = [
      "ecs:UpdateService",
    ]
    resources = [
      module.github_actions.ecs_service.id
    ]
  }
}

resource "aws_iam_policy" "ecr_access" {
  name   = "${var.project_name}-ecr-access"
  policy = data.aws_iam_policy_document.ecr_access.json
}

resource "aws_iam_user_policy_attachment" "ecr_access" {
  policy_arn = aws_iam_policy.ecr_access.arn
  user       = aws_iam_user.cicd_user.name
}

data "aws_iam_policy_document" "ecr_push_access" {
  statement {
    sid = "EcrAuthorization"
    actions = [
      "ecr:CompleteLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:InitiateLayerUpload",
      "ecr:BatchCheckLayerAvailability",
      "ecr:PutImage",
    ]
    resources = [
      module.github_actions.ecr_repository.arn,
    ]
  }
}

resource "aws_iam_policy" "ecr_push_access" {
  name   = "${var.project_name}-ecr-push-access"
  policy = data.aws_iam_policy_document.ecr_push_access.json
}

resource "aws_iam_user_policy_attachment" "cicd_ecr_push_access" {
  policy_arn = aws_iam_policy.ecr_push_access.arn
  user       = aws_iam_user.cicd_user.name
}

resource "aws_secretsmanager_secret" "cicd_user_keys" {
  name = "${var.project_name}-cicd_user_keys"
}

resource "aws_secretsmanager_secret_version" "cicd_user_key_value" {
  secret_id     = aws_secretsmanager_secret.cicd_user_keys.id
  secret_string = jsonencode(local.cicd_user_keys)
}