module "github_actions" {
  source              = "./modules/github-actions-fargate"
  identifier          = var.project_name
  github_access_token = var.github_access_token
  github_organization = var.github_organization
  aws_region          = var.aws_region
  subnets             = aws_subnet.private_subnets[*].id
}