resource "aws_secretsmanager_secret" "github_access_token" {
  name = "${var.identifier}-github-access-token"
}

resource "aws_secretsmanager_secret_version" "github_access_token" {
  secret_id     = aws_secretsmanager_secret.github_access_token.id
  secret_string = var.github_access_token
}

resource "aws_secretsmanager_secret" "github_organization" {
  name = "${var.identifier}-github-organization"
}

resource "aws_secretsmanager_secret_version" "github_organization" {
  secret_id     = aws_secretsmanager_secret.github_organization.id
  secret_string = var.github_organization
}