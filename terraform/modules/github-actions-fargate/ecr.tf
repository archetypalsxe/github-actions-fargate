resource "aws_ecr_repository" "ecr_repository" {
  name                 = "${var.identifier}-repository"
  image_tag_mutability = "MUTABLE"
  force_delete         = "true"
}