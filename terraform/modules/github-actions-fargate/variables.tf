variable "identifier" {
  description = "Some kind of unique identifier that will be prefixed to all the resources"
  type        = string
}

variable "github_access_token" {
  description = "The GitHub Access Token that will only be stored in an AWS secret"
  type        = string
}

variable "github_organization" {
  description = "The GitHub organization to use, will only be stored in an AWS secret"
  type        = string
}

variable "ecr_tag" {
  description = "The ECR tag that we should be using in production"
  type        = string
  default     = "production"
}

variable "aws_region" {
  description = "The AWS region we are deploying in"
  type        = string
}

variable "subnets" {
  description = "List of subnets to deploy in"
  type        = list(string)
}