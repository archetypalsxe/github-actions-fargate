resource "aws_ecs_task_definition" "github_actions" {
  family                   = var.identifier
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  task_role_arn            = aws_iam_role.fargate_task_role.arn
  execution_role_arn       = aws_iam_role.fargate_execution_role.arn
  container_definitions = jsonencode([
    {
      name  = "${var.identifier}-runner"
      image = "${aws_ecr_repository.ecr_repository.repository_url}:${var.ecr_tag}"
      secrets = [
        {
          name = "ACCESS_TOKEN"
          valueFrom = aws_secretsmanager_secret_version.github_access_token.arn
        },
        {
          name = "ORGANIZATION"
          valueFrom = aws_secretsmanager_secret_version.github_organization.arn
        },
      ],
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.log_group.name
          awslogs-region        = var.aws_region
          awslogs-stream-prefix = "container-logs"
        }
      }
    }
  ])
}

resource "aws_ecs_service" "github_actions" {
  name            = "${var.identifier}-github-actions"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.github_actions.arn
  desired_count   = 1
  capacity_provider_strategy {
    base              = 1
    capacity_provider = "FARGATE"
    weight            = 100
  }
  network_configuration {
    subnets = var.subnets
  }
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.identifier}-github-actions"

  configuration {
    execute_command_configuration {
      kms_key_id = aws_kms_key.logging_key.arn
      logging    = "OVERRIDE"

      log_configuration {
        cloud_watch_encryption_enabled = true
        cloud_watch_log_group_name     = aws_cloudwatch_log_group.log_group.name
      }
    }
  }

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${var.identifier}-github-actions-cluster"
}

resource "aws_kms_key" "logging_key" {
  description = "Key used to encrypt logs fromthe cluster"
  tags = {
    Name = "${var.identifier}-github-actions-logging-key"
  }
}

resource "aws_ecs_cluster_capacity_providers" "fargate" {
  cluster_name       = aws_ecs_cluster.ecs_cluster.name
  capacity_providers = ["FARGATE"]
  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}