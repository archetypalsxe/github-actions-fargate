resource "aws_iam_role" "fargate_execution_role" {
  name               = "${var.identifier}-fargate-execution-role"
  assume_role_policy = data.aws_iam_policy_document.fargate_execution_role_assume_policy.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
    aws_iam_policy.fargate_execution_secret_access.arn
  ]
}

data "aws_iam_policy_document" "fargate_execution_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "fargate_execution_secret_access" {
  statement {
    sid = "SecretManagerAccess"
    actions = [
      "secretsmanager:GetSecretValue",
    ]
    resources = [
      aws_secretsmanager_secret.github_access_token.arn,
      aws_secretsmanager_secret.github_organization.arn,
    ]
  }
}

resource "aws_iam_policy" "fargate_execution_secret_access" {
  name   = "${var.identifier}-fargate-execution-secret-access"
  policy = data.aws_iam_policy_document.fargate_execution_secret_access.json
}

resource "aws_iam_role" "fargate_task_role" {
  name               = "${var.identifier}-fargate-task-role"
  assume_role_policy = data.aws_iam_policy_document.fargate_task_role_assume_policy.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
  ]
}


data "aws_iam_policy_document" "fargate_task_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com",
      ]
    }
  }
}
