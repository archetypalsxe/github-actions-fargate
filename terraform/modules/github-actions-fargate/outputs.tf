output "ecr_repository" {
  value = aws_ecr_repository.ecr_repository
}

output "ecs_service" {
  value = aws_ecs_service.github_actions
}