variable "aws_region" {
  description = "The AWS region we are deploying in"
  type        = string
}

variable "project_name" {
  description = "The name of this project"
  type        = string
  default     = "github-actions-fargate"
}

variable "github_access_token" {
  description = "The GitHub Access Token that will only be stored in an AWS secret"
  type        = string
}

variable "github_organization" {
  description = "The GitHub organization to use, will only be stored in an AWS secret"
  type        = string
}

variable "public_subnet_count" {
  description = "The number of public subnets to create"
  type        = number
  default     = 1
}

variable "private_subnet_count" {
  description = "The number of private subnets to create"
  type        = number
  default     = 1
}

variable "num_availability_zones" {
  description = "The number of availability zones in the given region"
  type        = number
  default     = 6 // us-east-1
}