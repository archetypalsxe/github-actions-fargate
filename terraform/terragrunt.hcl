locals {
  config = yamldecode(file("terragrunt-config.yaml"))
  project_name = get_env("PROJECT_NAME", local.config.project_name)
  github_access_token = get_env("GITHUB_ACCESS_TOKEN", local.config.github_access_token)
  github_organization = get_env("GITHUB_ORGANIZATION", local.config.github_organization)
}

inputs = {
  project_name = local.project_name
  aws_region = local.config.aws_region
  working_dir = get_terragrunt_dir()
  github_access_token = local.github_access_token
  github_organization = local.github_organization
}

dependencies {
  paths = []
}

terraform {
  extra_arguments "env" {
    commands = ["init", "apply", "refresh", "import", "plan", "taint", "untaint"]
  }

  extra_arguments "init_args" {
    commands = [
      "init"
    ]

    arguments = [
      "--reconfigure",
    ]
  }
}

# Dynamically generating the provider to allow/disallow Localstack
generate "provider" {
  path = "main.tf"
  if_exists = "overwrite"
  contents = <<-EOF
# THIS FILE IS CREATED WITH TERRAGRUNT
# CHANGES TO THIS FILE WILL BE OVERRIDEN
# THIS FILE SHOULD NOT BE IN SOURCE CONTROL
provider "aws" {
  region = var.aws_region
  default_tags {
    tags = {
      ProjectName = var.project_name
      ManagedBy   = "Terraform"
    }
  }
}

# Provided by Terragrunt
terraform {
  backend "s3" {}
}
EOF
}

remote_state {
  backend = "s3"

  config = {
    region = local.config.aws_region
    bucket  = "${local.project_name}-remote-state"
    key     = "terraform.tfstate"
    encrypt = true
    dynamodb_table = "${local.project_name}-remote-locks"
  }
}